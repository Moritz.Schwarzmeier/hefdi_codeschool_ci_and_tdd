from typing import Protocol


class HVAC(Protocol):
    def stop_heating(self) -> None:
        pass

    def stop_cooling(self) -> None:
        pass

    def stop_blowing(self) -> None:
        pass

    def start_heating(self) -> None:
        pass

    def start_cooling(self) -> None:
        pass

    def start_blowing(self) -> None:
        pass

    def temperature(self) -> int:
        pass


class Controller:
    # introduced the dependency from controller to hvac interface
    def __init__(self, hvac: HVAC) -> None:
        self.hvac = hvac
        self.set_idle(hvac)
        self.desired_temperature = 0

    def set_idle(self, hvac: HVAC) -> None:
        hvac.stop_heating()
        hvac.stop_cooling()
        hvac.stop_blowing()

    def check_temperature(self) -> None:
        if self.desired_temperature > self.hvac.temperature():
            self.make_it_warm()
        else:
            self.make_it_cold()

    def make_it_warm(self) -> None:
        self.hvac.start_heating()
        self.hvac.start_blowing()

    def make_it_cold(self) -> None:
        self.hvac.start_cooling()
        self.hvac.start_blowing()
