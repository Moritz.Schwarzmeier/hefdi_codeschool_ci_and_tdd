from hvac import Controller, HVAC


class HVACMock(HVAC):
    def __init__(self) -> None:
        self.cooling = True
        self.blowing = True
        self.heating = True
        self.room_temperature = 0

    def temperature(self) -> int:
        return self.room_temperature

    def is_heating(self) -> bool:
        return self.heating

    def is_cooling(self) -> bool:
        return self.cooling

    def is_blowing(self) -> bool:
        return self.blowing

    def stop_heating(self) -> None:
        self.heating = False

    def stop_cooling(self) -> None:
        self.cooling = False

    def stop_blowing(self) -> None:
        self.blowing = False

    def start_heating(self) -> None:
        self.heating = True

    def start_cooling(self) -> None:
        self.cooling = True

    def start_blowing(self) -> None:
        self.blowing = True


# 1. test
# given__when__then naming scheme
def test_when_instantiating__then_everything_is_off() -> None:
    # arrange
    hvac = HVACMock()

    # act
    sut = Controller(hvac)

    # assertion
    assert_is_idling(hvac)


def test_given_its_too_cold__when_checking_temperature__it_starts_heating() -> None:
    # arrange
    hvac = HVACMock()
    sut = Controller(hvac)
    make_it_too_cold(hvac, sut)

    # act
    sut.check_temperature()

    # assert
    assert_is_heating(hvac)


def test_given_its_too_hot__when_checking_temperature__it_starts_cooling() -> None:
    # arrange
    hvac = HVACMock()
    sut = Controller(hvac)
    make_it_too_hot(hvac, sut)

    # act
    sut.check_temperature()

    # assert
    assert_is_cooling(hvac)


def make_it_too_cold(hvac: HVACMock, sut: Controller) -> None:
    sut.desired_temperature = 19
    hvac.room_temperature = 18


def make_it_too_hot(hvac: HVACMock, sut: Controller) -> None:
    sut.desired_temperature = 19
    hvac.room_temperature = 20


def assert_is_heating(hvac: HVACMock) -> None:
    assert hvac.is_cooling() is False
    assert hvac.is_heating() is True
    assert hvac.is_blowing() is True


def assert_is_cooling(hvac: HVACMock) -> None:
    assert hvac.is_cooling() is True
    assert hvac.is_heating() is False
    assert hvac.is_blowing() is True


def assert_is_idling(hvac: HVACMock) -> None:
    assert hvac.is_cooling() is False
    assert hvac.is_heating() is False
    assert hvac.is_blowing() is False
