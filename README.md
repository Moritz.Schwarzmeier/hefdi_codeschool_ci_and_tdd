# HeFDI Code School 'Sustainable Research Software'

This is the repository for the event in December 2023.
It showcases **TDD for a small python project**.

**Please refer to:**  
Linxweiler, J., Gläser, D., Schwarzmeier, M., & Peters, S. (2023, July 6). HeFDI Code School: Sustainable Research Software - Continuous Integration and Test Driven Development. Zenodo. https://doi.org/10.5281/zenodo.10256859  
Find the **latest version**: https://doi.org/10.5281/zenodo.8119397
